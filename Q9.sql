SELECT
 i1.category_name,
 SUM(i.item_price) AS total_price
FROM
 item i
INNER JOIN
 item_category i1
ON
 i.category_id = i1.category_id
GROUP BY
 i.category_id
ORDER BY
 total_price DESC;
